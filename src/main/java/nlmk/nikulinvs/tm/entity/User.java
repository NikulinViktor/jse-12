package nlmk.nikulinvs.tm.entity;

import nlmk.nikulinvs.tm.enumerated.Role;
import nlmk.nikulinvs.tm.util.HashUtils;
import nlmk.nikulinvs.tm.util.HashUtils.*;

import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

public class User {

    private Long id = System.nanoTime();

    private String login;

    private String password;

    private String passwordhash;

    private Role role;

    private String firstName;

    private String middleName;

    private String surName;

    public User(String login, String password) {
        this.login = login;
        this.password = password;
        this.passwordhash = HashUtils.hashMD5(password);
    }

    public User(String login, String password, Role role) {
        this.login = login;
        this.password = password;
        this.passwordhash = HashUtils.hashMD5(password);
        this.role = role;
    }

    public User(String login, String firstName, String middleName, String surName, String password) {
        this.login = login;
        this.firstName = firstName;
        this.middleName = middleName;
        this.surName = surName;
        this.password = password;
        this.passwordhash = HashUtils.hashMD5(password);
    }

    public User(String login, String firstName, String middleName, String surName, String password, Role role) {
        this.login = login;
        this.firstName = firstName;
        this.middleName = middleName;
        this.surName = surName;
        this.password = password;
        this.passwordhash = HashUtils.hashMD5(password);
        this.role = role;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
        this.passwordhash = HashUtils.hashMD5(password);
    }

    public String getPasswordhash() {
        return passwordhash;
    }

    public void setPasswordhash(String passwordhash) {
        this.passwordhash = passwordhash;
    }

    public Role getRole() {
        return role;
    }

    public String getDisplayName() {
        return role.getDisplayName();
    }

    public String[] getActions() {
        return role.getActions();
    }

    public void setRole(Role role) {
        this.role = role;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getMiddleName() {
        return middleName;
    }

    public void setMiddleName(String middleName) {
        this.middleName = middleName;
    }

    public String getSurName() {
        return surName;
    }

    public void setSurName(String surName) {
        this.surName = surName;
    }
}
