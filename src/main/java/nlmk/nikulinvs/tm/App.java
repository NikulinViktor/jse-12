package nlmk.nikulinvs.tm;


import nlmk.nikulinvs.tm.controller.ProjectController;
import nlmk.nikulinvs.tm.controller.TaskController;
import nlmk.nikulinvs.tm.controller.UserController;
import nlmk.nikulinvs.tm.controller.SystemController;
import nlmk.nikulinvs.tm.enumerated.Role;
import nlmk.nikulinvs.tm.exception.*;
import nlmk.nikulinvs.tm.repository.ProjectRepository;
import nlmk.nikulinvs.tm.repository.TaskRepository;
import nlmk.nikulinvs.tm.repository.UserRepository;
import nlmk.nikulinvs.tm.service.ProjectService;
import nlmk.nikulinvs.tm.service.ProjectTaskService;
import nlmk.nikulinvs.tm.service.TaskService;
import nlmk.nikulinvs.tm.service.UserService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import static nlmk.nikulinvs.tm.constant.TerminalConst.*;
import static nlmk.nikulinvs.tm.constant.Settings.MAX_LOG_SIZE;

import java.util.Arrays;
import java.util.Scanner;



public class App {

    private static final Logger logger = LogManager.getLogger(App.class);

    private final ProjectRepository projectRepository = new ProjectRepository();

    private final TaskRepository taskRepository = new TaskRepository();

    private final UserRepository userRepository = new UserRepository();

    private final ProjectService projectService = new ProjectService(projectRepository);

    private final TaskService taskService = new TaskService(taskRepository);

    private final ProjectTaskService projectTaskService = new ProjectTaskService(projectRepository, taskRepository);

    private final UserService userService = new UserService(userRepository, projectRepository, taskRepository);

    private final ProjectController projectController = new ProjectController(projectService, userService,projectTaskService);

    private final TaskController taskController = new TaskController(taskService, projectTaskService, userService);

    private final UserController userController = new UserController(userService);

    private final SystemController systermController = new SystemController();

    {
        projectRepository.create("PROJECT_A", "DESCRIPTION_PROJECT_ADMIN");
        projectRepository.create("PROJECT_T", "DESCRIPTION_PROJECT_TEST");
        taskRepository.create("TASK_A", "DESCRIPTION_TASK_ADMIN");
        taskRepository.create("TASK_T", "DESCRIPTION_TASK_TEST");
        userRepository.create("nikulin_vs", "Viktor", "  ", "NIkulin", "11111", Role.ADMIN);
        userRepository.create("r2d2", "robot", "droid", "starwars", "00000", Role.USER);
    }

    public static void main(String[] args) /*throws ProjectNotFoundException, TaskNotFoundException*/ {
        final Scanner scanner = new Scanner(System.in);
        logger.info(Scanner.class);
        final App app = new App();
        //app.run(args);
        app.systermController.displayWelcome();
        String command = "";
        while (!CMD_EXIT.equals(command)) {
            command = scanner.nextLine();
            try{
                logger.trace(command);
            app.run(command);
            }
            catch (ProjectNotFoundException | TaskNotFoundException pt) {
                logger.error(pt.getMessage());
                pt.printStackTrace();
            }
            System.out.println();
        }
    }

    public void run(final String[] args) throws ProjectNotFoundException, TaskNotFoundException {
        if (args == null) {
            return;
        }
        if (args.length < 1) {
            return;
        }
        final String param = args[0];
        final int result = run(param);
        System.exit(result);
    }

    public int run(final String param) throws ProjectNotFoundException, TaskNotFoundException {
        if (param == null || param.isEmpty()) {
            return -1;
        }
        systermController.logCommand.add(param);
        if (systermController.logCommand.size() > MAX_LOG_SIZE) {
            systermController.logCommand.poll();
        }
        if (!Arrays.asList(ALL_ACTIONS).contains(param)) {
            if (userController.checkAuthorisation(param) == -1) {
                return -1;
            }
        }
        switch (param) {
            case LOG_ON: return userController.registry();
            case LOG_OFF: return userController.logOff();
            case USER_INFO: return userController.displayUserInfo();
            case USER_UPDATE: return userController.updateUser();
            case USER_UPDATE_PASSWORD: return userController.updatePassword();

            case CMD_VERSION: return systermController.displayVersion();
            case CMD_ABOUT: return systermController.displayAbout();
            case CMD_HELP: return systermController.displayHelp();
            case CMD_EXIT: return systermController.displayExit();
            case SHOW_LOG_COMMANDS: return systermController.showLogHistory();

            case CMD_PROJECT_CREATE: return projectController.createProject();
            case CMD_PROJECT_CLEAR: return projectController.clearProject();
            case CMD_PROJECT_LIST: return projectController.listProject();
            case PROJECT_VIEW_BY_ID: return projectController.viewProjectById();
            case PROJECT_VIEW_BY_INDEX: return projectController.viewProjectByIndex();
            case PROJECT_VIEW_BY_NAME: return projectController.viewProjectByName();
            case PROJECT_MAP_VIEW_BY_NAME:return projectController.viewProjectMapByName();
                /*try {
                    return projectController.viewProjectMapByName();
                } catch (ProjectNotFoundException e) {
                    e.printStackTrace();
                }*/
            case CMD_PROJECT_REMOVE_BY_ID: return projectController.removeProjectById();
            case PROJECT_REMOVE_BY_INDEX: return projectController.removeProjectByIndex();
            case PROJECT_REMOVE_BY_NAME: return projectController.removeProjectByName();
            case PROJECT_UPDATE_BY_ID: return projectController.updateProjectById();
            case CMD_PROJECT_REMOVE_BY_INDEX: return projectController.updateProjectByIndex();
            case PROJECT_UPDATE_BY_NAME: return projectController.updateProjectByName();

            case CMD_TASK_CREATE: return taskController.createTask();
            case CMD_TASK_CLEAR: return taskController.clearTask();
            case CMD_TASK_LIST: return taskController.listTask();
            case TASK_VIEW_BY_ID: return taskController.viewTaskById();
            case TASK_VIEW_BY_INDEX: return taskController.viewTaskByIndex();
            case TASK_VIEW_BY_NAME: return taskController.viewTaskByName();
            case TASK_MAP_VIEW_BY_NAME:return taskController.viewTaskMapByName();
                /*try {
                    return taskController.viewTaskMapByName();
                } catch (TaskNotFoundException e) {
                    e.printStackTrace();
                }*/
            case CMD_TASK_REMOVE_BY_ID: return taskController.removeTaskById();
            case CMD_TASK_REMOVE_BY_INDEX: return taskController.removeTaskByIndex();
            case CMD_TASK_REMOVE_BY_NAME: return taskController.removeTaskByName();
            case TASK_UPDATE_BY_ID: return taskController.updateTaskById();
            case CMD_TASK_UPDATE_BY_INDEX: return taskController.updateTaskByIndex();
            case TASK_UPDATE_BY_NAME: return taskController.updateTaskByName();
            case CMD_TASK_ADD_TO_PROJECT_BY_IDS: return taskController.addTaskToProjectByIds();
            case CMD_TASK_REMOVE_FROM_PROJECT_BY_IDS: return taskController.removeTaskToProjectByIds();
            case CMD_TASK_LIST_BY_PROJECT_ID: return taskController.listTaskByProjectId();

            case USER_CREATE: return userController.createUser();
            case USER_CLEAR: return userController.clearUser();
            case USER_LIST: return userController.listUser();
            case USER_VIEW_BY_ID: return userController.viewUserById();
            case USER_VIEW_BY_INDEX: return userController.viewUserByIndex();
            case USER_VIEW_BY_LOGIN: return userController.viewUserByLogin();
            case USER_REMOVE_BY_ID: return userController.removeUserById();
            case USER_REMOVE_BY_INDEX: return userController.removeUserByIndex();
            case USER_REMOVE_BY_LOGIN: return userController.removeUserByLogin();
            case USER_UPDATE_BY_ID: return userController.updateUserById();
            case USER_UPDATE_BY_INDEX: return userController.updateUserByIndex();
            case USER_UPDATE_BY_LOGIN: return userController.updateUserByLogin();
            case USER_ASSIGN_PROJECT: return userController.addUsertoProjectbyIds();
            case USER_REMOVE_PROJECT: return userController.removeUsertoProjectbyIds();
            case USER_ASSIGN_TASK: return userController.addUsertoTaskbyIds();
            default: return systermController.displayError();
        }

    }

}
