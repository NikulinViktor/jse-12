package nlmk.nikulinvs.tm.constant;

public class TerminalConst {
    //Help commands:
    public static final String CMD_HELP = "help";
    public static final String CMD_VERSION = "version";
    public static final String CMD_ABOUT = "about";
    public static final String CMD_EXIT = "exit";
    //Project control
    public static final String CMD_PROJECT_CREATE = "project-create";
    public static final String CMD_PROJECT_CLEAR = "project-clear";
    public static final String CMD_PROJECT_LIST = "project-list";
    public static final String PROJECT_VIEW_BY_ID = "project-view-by-id";
    public static final String PROJECT_VIEW_BY_INDEX = "project-view-by-index";
    public static final String PROJECT_VIEW_BY_NAME = "project-view-by-name";
    public static final String PROJECT_MAP_VIEW_BY_NAME = "project-map-view-by-name";
    public static final String CMD_PROJECT_REMOVE_BY_ID = "project-remove-by-id";
    public static final String PROJECT_REMOVE_BY_INDEX = "project-remove-by-index";
    public static final String PROJECT_REMOVE_BY_NAME = "project-remove-by-name";
    public static final String PROJECT_UPDATE_BY_ID = "project-update-by-id";
    public static final String CMD_PROJECT_REMOVE_BY_INDEX = "project-update-by-index";
    public static final String PROJECT_UPDATE_BY_NAME = "project-update-by-name";
    //Task control
    public static final String CMD_TASK_CREATE = "task-create";
    public static final String CMD_TASK_CLEAR = "task-clear";
    public static final String CMD_TASK_LIST = "task-list";
    public static final String TASK_VIEW_BY_ID = "task-view-by-id";
    public static final String TASK_VIEW_BY_INDEX = "task-view-by-index";
    public static final String TASK_VIEW_BY_NAME = "task-view-by-name";
    public static final String TASK_MAP_VIEW_BY_NAME = "task-map-view-by-name";
    public static final String CMD_TASK_REMOVE_BY_ID = "task-remove-by-id";
    public static final String CMD_TASK_REMOVE_BY_INDEX = "task-remove-by-index";
    public static final String CMD_TASK_REMOVE_BY_NAME = "task-remove-by-name";
    public static final String TASK_UPDATE_BY_ID = "task-update-by-id";
    public static final String CMD_TASK_UPDATE_BY_INDEX = "task-update-by-index";
    public static final String TASK_UPDATE_BY_NAME = "task-update-by-name";
    public static final String CMD_TASK_LIST_BY_PROJECT_ID = "task-list-by-project-id";
    public static final String CMD_TASK_ADD_TO_PROJECT_BY_IDS = "task-add-to-project-by-ids";
    public static final String CMD_TASK_REMOVE_FROM_PROJECT_BY_IDS = "task-remove-from-project-by-ids";
    //Auth control
    public static final String LOG_ON = "log-on";
    public static final String LOG_OFF = "log-off";
    public static final String USER_INFO = "user-info";
    public static final String USER_UPDATE = "user-update";
    public static final String USER_UPDATE_PASSWORD = "user-update-password";
    //User control
    public static final String USER_CREATE = "user-create";
    public static final String USER_CLEAR = "user-clear";
    public static final String USER_LIST = "user-list";
    public static final String USER_VIEW_BY_ID = "user-view-by-id";
    public static final String USER_VIEW_BY_INDEX = "user-view-by-index";
    public static final String USER_VIEW_BY_LOGIN = "user-view-by-login";
    public static final String USER_REMOVE_BY_ID = "user-remove-by-id";
    public static final String USER_REMOVE_BY_INDEX = "user-remove-by-index";
    public static final String USER_REMOVE_BY_LOGIN = "user-remove-by-login";
    public static final String USER_UPDATE_BY_ID = "user-update-by-id";
    public static final String USER_UPDATE_BY_INDEX = "user-update-by-index";
    public static final String USER_UPDATE_BY_LOGIN = "user-update-by-login";
    public static final String USER_ASSIGN_PROJECT = "user-assign-project";
    public static final String USER_REMOVE_PROJECT = "user-remove-project";
    public static final String USER_ASSIGN_TASK = "user-assign-task";
    public static final String USER_REMOVE_TASK = "user-remove-task";
    //Log
    public static final String SHOW_LOG_COMMANDS = "show-log";


    public static String[] ALL_ACTIONS = new String[]{
            USER_CREATE, LOG_ON, LOG_OFF,
            CMD_ABOUT, CMD_HELP, CMD_VERSION, CMD_EXIT,SHOW_LOG_COMMANDS };

    public static String[] ADMIN_ACTIONS = new String[]{
            LOG_ON, LOG_OFF, USER_INFO, USER_UPDATE, USER_UPDATE_PASSWORD,
            USER_CREATE, USER_CLEAR, USER_LIST,
            USER_VIEW_BY_LOGIN, USER_VIEW_BY_ID,
            USER_REMOVE_BY_LOGIN, USER_REMOVE_BY_ID,
            USER_UPDATE_BY_LOGIN, USER_UPDATE_BY_ID,
            CMD_ABOUT, CMD_HELP, CMD_VERSION,
            CMD_PROJECT_CREATE, CMD_TASK_CREATE,
            CMD_PROJECT_CLEAR, CMD_TASK_CLEAR,
            CMD_PROJECT_LIST, CMD_TASK_LIST,
            PROJECT_VIEW_BY_INDEX, TASK_VIEW_BY_INDEX,
            PROJECT_MAP_VIEW_BY_NAME,PROJECT_VIEW_BY_NAME,
            PROJECT_UPDATE_BY_NAME, TASK_UPDATE_BY_NAME,
            PROJECT_VIEW_BY_ID, TASK_VIEW_BY_ID,
            PROJECT_REMOVE_BY_INDEX, CMD_TASK_REMOVE_BY_INDEX,
            CMD_PROJECT_REMOVE_BY_ID, CMD_TASK_REMOVE_BY_ID,
            PROJECT_REMOVE_BY_NAME, CMD_TASK_REMOVE_BY_NAME,
            CMD_PROJECT_REMOVE_BY_INDEX, CMD_TASK_UPDATE_BY_INDEX,
            TASK_MAP_VIEW_BY_NAME,TASK_VIEW_BY_NAME,
            PROJECT_UPDATE_BY_ID, TASK_UPDATE_BY_ID,
            CMD_TASK_LIST_BY_PROJECT_ID,
            CMD_TASK_ADD_TO_PROJECT_BY_IDS,
            CMD_TASK_REMOVE_FROM_PROJECT_BY_IDS,
            USER_ASSIGN_PROJECT, USER_REMOVE_PROJECT, USER_ASSIGN_TASK, USER_REMOVE_TASK };

    public static String[] USER_ACTIONS = new String[]{
            LOG_ON, LOG_OFF,
            USER_CREATE, USER_UPDATE_PASSWORD,
            USER_INFO, USER_UPDATE,
            CMD_ABOUT, CMD_HELP, CMD_VERSION,
            CMD_PROJECT_CREATE, CMD_TASK_CREATE,
            CMD_PROJECT_CLEAR, CMD_TASK_CLEAR,
            CMD_PROJECT_LIST, CMD_TASK_LIST,
            PROJECT_VIEW_BY_INDEX, TASK_VIEW_BY_INDEX,
            PROJECT_VIEW_BY_ID, TASK_VIEW_BY_ID,
            PROJECT_REMOVE_BY_INDEX, CMD_TASK_REMOVE_BY_INDEX,
            CMD_PROJECT_REMOVE_BY_ID, CMD_TASK_REMOVE_BY_ID,
            PROJECT_REMOVE_BY_NAME, CMD_TASK_REMOVE_BY_NAME,
            CMD_PROJECT_REMOVE_BY_INDEX, CMD_TASK_UPDATE_BY_INDEX,
            PROJECT_UPDATE_BY_ID, TASK_UPDATE_BY_ID,
            CMD_TASK_LIST_BY_PROJECT_ID,
            CMD_TASK_REMOVE_FROM_PROJECT_BY_IDS,
            USER_ASSIGN_PROJECT, USER_REMOVE_PROJECT, USER_ASSIGN_TASK, USER_REMOVE_TASK };

    }
