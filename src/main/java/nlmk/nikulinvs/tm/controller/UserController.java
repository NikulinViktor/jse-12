package nlmk.nikulinvs.tm.controller;

import nlmk.nikulinvs.tm.App;
import nlmk.nikulinvs.tm.entity.User;
import nlmk.nikulinvs.tm.enumerated.Role;
import nlmk.nikulinvs.tm.service.UserService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.Arrays;
import java.util.List;

public class UserController extends AbstractController {
    private static final Logger logger = LogManager.getLogger(UserController.class);
    private final UserService userService;
    private String RoleAuth = "ADMIN";

    public UserController(UserService userService) {
        this.userService = userService;
    }

    public int createUser() {
        System.out.println("[CREATE USER]");
        System.out.println("PLEASE, ENTER LOGIN:");
        final String login = scanner.nextLine();
        System.out.println("PLEASE, ENTER FIRSTNAME:");
        final String firstName = scanner.nextLine();
        System.out.println("PLEASE, ENTER MIDDLE NAME:");
        final String middleName = scanner.nextLine();
        System.out.println("PLEASE, ENTER SURNAME:");
        final String surName = scanner.nextLine();
        System.out.println("PLEASE, ENTER PASSWORD:");
        String password = scanner.nextLine();
        System.out.println("PLEASE, ENTER ROLE:");
        String role = scanner.nextLine();
        userService.create(login, firstName, middleName, surName, password, role);
        System.out.println("[OK]");
        return 0;
    }

    public int updateUserById() {
        System.out.println("[UPDATING A USER BY ID]");
        System.out.println("PLEASE ENTER, USER ID:");
        final long id = scanner.nextLong();
        final User user = userService.findById(id);
        if (user == null) {
            System.out.println("[FAIL]");
            return 0;
        }
        System.out.println("PLEASE, ENTER LOGIN:");
        final String login = scanner.nextLine();
        System.out.println("PLEASE, ENTER FIRST NAME:");
        final String firstName = scanner.nextLine();
        System.out.println("PLEASE, ENTER MIDDLE NAME:");
        final String middleName = scanner.nextLine();
        System.out.println("PLEASE, ENTER SURNAME:");
        final String surName = scanner.nextLine();
        System.out.println("PLEASE, ENTER PASSWORD:");
        final String password = scanner.nextLine();
        System.out.println("PLEASE, ENTER ROLE:");
        final String role = scanner.nextLine();
        userService.update(user.getId(), login, firstName, middleName, surName, password, role);
        System.out.println("[OK]");
        return 0;
    }

    public int updateUserByIndex() {
        System.out.println("[UPDATING A USER BY INDEX]");
        System.out.println("PLEASE ENTER, USER INDEX:");
        final int index = Integer.parseInt(scanner.nextLine()) - 1;
        final User user = userService.findByIndex(index);
        if (user == null) {
            System.out.println("[FAIL]");
            return 0;
        }
        System.out.println("PLEASE, ENTER LOGIN:");
        final String login = scanner.nextLine();
        System.out.println("PLEASE, ENTER FIRST NAME:");
        final String firstName = scanner.nextLine();
        System.out.println("PLEASE, ENTER MIDDLE NAME:");
        final String middleName = scanner.nextLine();
        System.out.println("PLEASE, ENTER SURNAME:");
        final String surName = scanner.nextLine();
        System.out.println("PLEASE, ENTER PASSWORD:");
        final String password = scanner.nextLine();
        System.out.println("PLEASE, ENTER ROLE:");
        final String role = scanner.nextLine();
        userService.update(user.getId(), login, firstName, middleName, surName, password, role);
        System.out.println("[OK]");
        return 0;
    }

    public int updateUserByLogin() {
        System.out.println("[UPDATING A USER BY LOGIN]");
        System.out.println("PLEASE ENTER, USER LOGIN:");
        final String login = scanner.nextLine();
        final User user = userService.findByLogin(login);
        if (user == null) {
            System.out.println("[FAIL]");
            return 0;
        }
        System.out.println("PLEASE, ENTER FIRST NAME:");
        final String firstName = scanner.nextLine();
        System.out.println("PLEASE, ENTER MIDDLE NAME:");
        final String middleName = scanner.nextLine();
        System.out.println("PLEASE, ENTER SURNAME:");
        final String surName = scanner.nextLine();
        System.out.println("PLEASE, ENTER PASSWORD:");
        final String password = scanner.nextLine();
        System.out.println("PLEASE, ENTER ROLE:");
        final String role = scanner.nextLine();
        userService.update(user.getId(), login, firstName, middleName, surName, password, role);
        System.out.println("[OK]");
        return 0;
    }

    public int removeUserById() {
        System.out.println("[REMOVE A USER BY ID]");
        System.out.println("PLEASE, ENTER USER ID:");
        final long id = scanner.nextLong();
        final User user = userService.removeById(id);
        if (user == null) System.out.println("[FAIL]");
        else System.out.println("[OK]");
        return 0;
    }

    public int removeUserByIndex() {
        System.out.println("[REMOVE A USER BY ID]");
        System.out.println("PLEASE, ENTER USER INDEX:");
        final int index = scanner.nextInt() - 1;
        final User user = userService.removeByIndex(index);
        if (user == null) System.out.println("[FAIL]");
        else System.out.println("[OK]");
        return 0;
    }

    public int removeUserByLogin() {
        System.out.println("[REMOVE USER BY LOGIN]");
        System.out.println("PLEASE, ENTER LOGIN:");
        final String login = scanner.nextLine();
        final User user = userService.removeByLogin(login);
        if (user == null) System.out.println("[FAIL]");
        else System.out.println("[OK]");
        return 0;
    }

    public int clearUser() {
        System.out.println("[USERS CLEAR]");
        userService.clear();
        System.out.println("[OK]");
        return 0;
    }

    public void viewUser(final User user) {
        if (user == null) return;
        System.out.println("[VIEW USER]");
        System.out.println("ID: " + user.getId());
        System.out.println("LOGIN: " + user.getLogin());
        System.out.println("FIRST NAME: " + user.getFirstName());
        System.out.println("MIDDLE NAME: " + user.getMiddleName());
        System.out.println("SURNAME: " + user.getSurName());
        System.out.println("PASSWORD: " + user.getPassword());
        System.out.println("PASSWORD HASH: " + user.getPasswordhash());
        System.out.println("ROLE: " + user.getRole());
        System.out.println("[OK]");
    }

    public void viewUsers(List<User> users) {
        if (users == null || users.isEmpty()) return;
        int index = 1;
        for (final User user : users) {
            System.out.println(index + ". " + user.getId() + ": " + user.getFirstName());
            index++;
        }
    }

    public int viewUserById() {
        System.out.println("ENTER, USER ID:");
        final long id = scanner.nextLong();
        final User user = userService.findById(id);
        viewUser(user);
        return 0;
    }

    public int viewUserByIndex() {
        System.out.println("ENTER, USER INDEX:");
        final int index = scanner.nextInt() - 1;
        final User user = userService.findByIndex(index);
        viewUser(user);
        return 0;
    }

    public int viewUserByLogin() {
        System.out.println("ENTER, USER LOGIN:");
        final String login = scanner.nextLine();
        final User user = userService.findByLogin(login);
        viewUser(user);
        return 0;
    }

    public int listUser() {
        System.out.println("[USERS LIST]");
        viewUsers(userService.findAll());
        System.out.println("[OK]");
        return 0;
    }

    public int checkAuthorisation(final String param) {
        if (!Arrays.asList(Role.ADMIN.getActions()).contains(param)) {
            System.out.println("ERROR CMMAND.");
            System.out.println("ENTER help TO VIEW AVAILABLE COMMANDS");
            System.out.println("ATTENTION!!! -> ALL COMMANDS MUST BE ENTERED ON LOWER CASE");
            return -1;
        }
        if (userService.currentUser == null) {
            System.out.println("YOU ARE HAVE NOT PERMISSIONS FOR THIS COMMAND");
            System.out.println("PLEASE, LOG ON TASK MANAGER");
            return -1;
        }
        if (!Arrays.asList(userService.currentUser.getRole().getActions()).contains(param)) {
            System.out.println("NOT ALLOWED");
            return -1;
        }
        return 0;
    }

    public int registry() {
        System.out.println("[ENTER LOGIN:]");
        final String login = scanner.nextLine();
        final User user = userService.findByLogin(login);
        if (user == null) {
            logger.warn("Trying log on system with incorrect login = " + login);
            logger.warn("LOGIN " + login + " IS NOT FOUND IN SYSTEM ");
            //System.out.println("LOGIN IS NOT FOUND IN SYSTEM");
            return 0;
        }
        System.out.println("[ENTER PASSWORD:]");
        if (userService.checkPassword(user, scanner.nextLine())) {
            logger.info("User log on system as " + login + " with role " + user.getRole().name());
            //System.out.println("HI! YOU ARE LOGED AS: " + "|LOGIN: " +user.getLogin()+ " |" + "  " + "|ROLE:  "+ user.getRole().name()+" |");
            userService.currentUser = user;
            if (user.getRole().name().equals(RoleAuth)) {
                System.out.println("You have permissions for all existing commans");
            }
            else {
                System.out.println("Role USER has limited rights");
            }
        } else {
            logger.error("Authorisation Error. Password incorrect.");
            //System.out.println("FAIL");
        }
        return 0;
    }

    public int logOff() {
        logger.info("Login session ended ");
        userService.currentUser = null;
        //System.out.println("LOG OFF");
        return 0;
    }

    public int displayUserInfo() {
        viewUser(userService.currentUser);
        return 0;
    }

    public int updateUser() {
        final String login;
        final User user;
        if (userService.currentUser == null) {
            System.out.println("FAIL");
        } else {
            if (userService.currentUser.getDisplayName().equals(Role.ADMIN.getDisplayName())) {
                System.out.println("[UPDATE INFORMATION OF ANY USER]");
                System.out.println("PLEASE ENTER, USER LOGIN:");
                login = scanner.nextLine();
                user = userService.findByLogin(login);
                if (user == null) {
                    System.out.println("[FAIL]");
                    return 0;
                }
            } else {
                System.out.println("[UPDATE INFORMATION OF CURRENT USER]");
                login = userService.currentUser.getLogin();
            }
            System.out.println("[ENTER PASSWORD]");
            final String password = scanner.nextLine();
            System.out.println("[ENTER FIRST NAME:]");
            final String firstName = scanner.nextLine();
            System.out.println("[ENTER MIDDLE NAME:]");
            final String middleName = scanner.nextLine();
            System.out.println("[ENTER LAST NAME]");
            final String lastName = scanner.nextLine();
            userService.updateByLogin(login, firstName, middleName, lastName, password);
            System.out.println("[OK]");
        }
        return 0;
    }

    public int updatePassword() {
        final String login;
        final User user;
        if (userService.currentUser == null) {
            System.out.println("[FAIL]");
        } else {
            if (userService.currentUser.getDisplayName().equals(Role.ADMIN.getDisplayName())) {
                System.out.println("[UPDATE PASSWORD OF ANY USER]");
                System.out.println("PLEASE, ENTER USER LOGIN:");
                login = scanner.nextLine();
                user = userService.findByLogin(login);
                if (user == null) {
                    System.out.println("[FAIL]");
                    return 0;
                }
            } else {
                System.out.println("[UPDATE PASSWORD OF CURRENT USER]");
                login = userService.currentUser.getLogin();
                user = userService.currentUser;
            }
            System.out.println("[ENTER OLD PASSWORD:]");
            final String oldPassword = scanner.nextLine();
            if (userService.checkPassword(user, oldPassword)) {
                System.out.println("[ENTER NEW PASSWORD:]");
                final String newPassword = scanner.nextLine();
                userService.updatePasswordByLogin(login, newPassword);
                System.out.println("[OK]");
            } else {
                System.out.println("[PASSWORD INCORRECT]");
            }
        }
        return 0;
    }

    public int addUsertoProjectbyIds() {
        System.out.println("[ADD USER TO PROJECT BY IDS]");
        System.out.println("PLEASE, ENTER PROJECT ID:");
        final long projectId = scanner.nextLong();
        userService.addUserToProject(projectId, userService.currentUser.getId());
        System.out.println("[OK]");
        return 0;
    }

    public int removeUsertoProjectbyIds() {
        System.out.println("[REMOVE USER FROM PROJECT BY IDS]");
        System.out.println("PLEASE, ENTER PROJECT ID:");
        final long projectId = scanner.nextLong();
        userService.removeUserFromProject(projectId, userService.currentUser.getId());
        System.out.println("[OK]");
        return 0;
    }

    public  int addUsertoTaskbyIds() {
        System.out.println("[ADD USER TO TASK BY IDS]");
        System.out.println("PLEASE, ENTER TASK ID:");
        final long taskId = scanner.nextLong();
        userService.addUserToTask(taskId, userService.currentUser.getId());
        System.out.println("[OK]");
        return 0;
    }

    public int removeUsertoTaskbyIds() {
        System.out.println("[REMOVE USER FROM PROJECT BY IDS]");
        System.out.println("PLEASE, ENTER TASK ID:");
        final long taskId = scanner.nextLong();
        userService.removeUserFromTask(taskId, userService.currentUser.getId());
        System.out.println("[OK]");
        return 0;

    }
}
