package nlmk.nikulinvs.tm.controller;

import nlmk.nikulinvs.tm.entity.Project;
import nlmk.nikulinvs.tm.entity.Task;
import nlmk.nikulinvs.tm.enumerated.Role;
import nlmk.nikulinvs.tm.service.ProjectService;
import nlmk.nikulinvs.tm.service.UserService;
import nlmk.nikulinvs.tm.exception.ProjectNotFoundException;
import nlmk.nikulinvs.tm.service.ProjectTaskService;


import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class ProjectController extends AbstractController {

    private final ProjectService projectService;
    private final ProjectTaskService projectTaskService;
    private final UserService userService;

    public ProjectController(ProjectService projectService, UserService userService,
                             ProjectTaskService projectTaskService) {
        this.projectService = projectService;
        this.projectTaskService = projectTaskService;
        this.userService = userService;
    }

    public int createProject() {
        System.out.println("[CREATE PROJECT]");
        System.out.println("PLEASE, ENTER PROJECT NAME:");
        final String name = scanner.nextLine();
        System.out.println("PLEASE, ENTER PROJECT DESCRIPTION:");
        final String description = scanner.nextLine();
        projectService.create(name, description, userService.currentUser.getId());
        System.out.println("[OK]");
        return 0;
    }

public int updateProjectById() throws ProjectNotFoundException {
    System.out.println("[UPDATE PROJECT]");
    System.out.println("[PLEASE, ENTER PROJECT ID:]");
    final Long id = scanner.nextLong();
    final Long userId = userService.currentUser.getId();
    final Project project = projectService.findById(id, userId);
    if (project == null) {
        throw new ProjectNotFoundException("[PROJECT IS NOT FOUND BY ID. UPDATE FAILED!");
    } else {
        System.out.println("[PLEASE, ENTER PROJECT NAME:]");
        final String name = scanner.nextLine();
        System.out.println("[PLEASE, ENTER PROJECT DESCRIPTION:]");
        final String description = scanner.nextLine();
        projectService.update(project.getId(), name, description, userId);
        System.out.println("[OK]");
    }
    return 0;
}

    public int updateProjectByIndex() throws ProjectNotFoundException {
        System.out.println("[UPDATE PROJECT]");
        System.out.println("ENTER, PROJECT INDEX:");
        final int index = Integer.parseInt(scanner.nextLine()) - 1;
        final Project project = projectService.findByIndex(index,userService.currentUser.getId());
        if (project == null) {
            System.out.println("[FAIL]");
            return 0;
        }
        System.out.println("PLEASE, ENTER PROJECT NAME:");
        final String name = scanner.nextLine();
        System.out.println("PLEASE, ENTER PROJECT DESCRIPTION:");
        final String description = scanner.nextLine();
        projectService.update(project.getId(), name, description,userService.currentUser.getId());
        System.out.println("[OK]");
        return 0;
    }

    public int updateProjectByName() throws ProjectNotFoundException {
        System.out.println("[UPDATE PROJECT]");
        System.out.println("ENTER, PROJECT NAME:");
        final String name = scanner.nextLine();
        final Project project = projectService.findByName(name);
        if (project == null) {
            System.out.println("[FAIL]");
            return 0;
        }
        System.out.println("PLEASE, ENTER PROJECT NAME:");
        final String new_name = scanner.nextLine();
        System.out.println("PLEASE, ENTER PROJECT DESCRIPTION:");
        final String description = scanner.nextLine();
        projectService.update(project.getId(), new_name, description,userService.currentUser.getId());
        System.out.println("[OK]");
        return 0;
    }

    public int removeProjectById() throws ProjectNotFoundException {
        System.out.println("[REMOVE PROJECT BY ID]");
        System.out.println("[PLEASE, ENTER PROJECT ID:]");
        final Long id = scanner.nextLong();
        final Project project = projectService.removeById(id, userService.currentUser.getId());
        if (project == null) {
                throw new ProjectNotFoundException("[PROJECT IS NOT FOUND. REMOVE FAILED!");
         } else {
        final List<Task> tasks = projectTaskService.findAllByProjectId(project.getId());
        for (final Task task : tasks) {
            projectTaskService.removeTaskFromProject(project.getId(), task.getId());
            }
            System.out.println("[OK]");
                 }
        return 0;
    }

    public int removeProjectByIndex() throws ProjectNotFoundException {
        System.out.println("[REMOVE PROJECT BY INDEX]");
        System.out.println("PLEASE, ENTER PROJECT INDEX:");
        final int index = scanner.nextInt()-1;
        final Project project = projectService.removeByIndex(index,userService.currentUser.getId());
        if (project == null) {
            throw new ProjectNotFoundException("Project with index = " + (index+1) + " are not exist");
        }
             //System.out.println("[FAIL]");
        else System.out.println("[OK]");
        return 0;
    }

    public int removeProjectByName() {
        System.out.println("[REMOVE PROJECT BY NAME]");
        System.out.println("PLEASE, ENTER PROJECT NAME:");
        final String name = scanner.nextLine();
        final Project project = projectService.removeByName(name);
        if (project == null) System.out.println("[FAIL]");
        else System.out.println("[OK]");
        return 0;
    }

    public int clearProject() {
        System.out.println("[CLEAR PROJECT]");
        projectService.clear();
        System.out.println("[OK]");
        return 0;
    }

    public void viewProject(final Project project) {
        if (project == null) return;
        System.out.println("[VIEW TASK]");
        System.out.println("ID: " + project.getId());
        System.out.println("NAME: " + project.getName());
        System.out.println("DESCRIPTION: " + project.getDescription());
        System.out.println("USER_ID: " + project.getUserId());
        System.out.println("[OK]");
    }

public void viewProjectsMap(final List<Project> projects) throws ProjectNotFoundException {
    if (projects == null || projects.isEmpty()) {
        throw new ProjectNotFoundException("[PROJECTS ARE NOT FOUND]");
    }
    int index = 1;
    Collections.sort(projects, new Comparator<Project>() {
        @Override
        public int compare(Project p1, Project p2) {
            return p1.getName().compareTo(p2.getName());
        }
    });
    for (final Project project : projects) {
        System.out.println(index + ". " + project.getId() + ": " + project.getName()+ ": " + project.getDescription());
        index++;
    }
    System.out.println("[OK]");
}

    public int viewProjectById() throws ProjectNotFoundException {
        System.out.println("ENTER, PROJECT ID:");
        final long id = scanner.nextLong();
        final Project project = projectService.findById(id,userService.currentUser.getId());
        viewProject(project);
        return 0;
    }

    public int viewProjectByIndex() throws ProjectNotFoundException {
        System.out.println("ENTER, PROJECT INDEX:");
        final int index = scanner.nextInt() - 1;
        final Project project = projectService.findByIndex(index,userService.currentUser.getId());
        viewProject(project);
        return 0;
    }

    public int viewProjectByName() {
        System.out.println("ENTER, PROJECT NAME:");
        final String name = scanner.nextLine();
        final Project project = projectService.findByName(name);
        viewProject(project);
        return 0;
    }
    //
    public int viewProjectMapByName() throws ProjectNotFoundException {
        System.out.println("[PLEASE, ENTER PROJECT NAME:]");
        final String name = scanner.nextLine();
        final List<Project> projects = projectService.findProjectMapByName(name, userService.currentUser.getId());
        viewProjectsMap(projects);
        return 0;
    }

    public int listProject() throws ProjectNotFoundException {
        System.out.println("[LIST PROJECT]");
        int index = 1;
        List<Project> projectList;
        if (userService.currentUser == null) {
            System.out.println("[FAIL]");
        } else {
            if (userService.currentUser.getDisplayName().equals(Role.ADMIN.getDisplayName()))
                projectList = projectService.findAllOrderByName();
            else
                projectList = projectService.findAllByUserId(userService.currentUser.getId());
            for (final Project project : projectList) {
                System.out.println("Index = "+index + ". "+
                                   "ID = "+ project.getId() + ": " + project.getName()+ ": " + project.getDescription()) ;
                index++;
            }
            if (index > 1) System.out.println("[OK]");
            else System.out.println("[PROJECTS ARE NOT FOUND]");
        }

        return 0;

    }


}
