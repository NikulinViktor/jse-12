package nlmk.nikulinvs.tm.controller;

import nlmk.nikulinvs.tm.entity.Project;
import nlmk.nikulinvs.tm.entity.Task;
import nlmk.nikulinvs.tm.exception.ProjectNotFoundException;
import nlmk.nikulinvs.tm.service.ProjectService;
import nlmk.nikulinvs.tm.service.ProjectTaskService;
import nlmk.nikulinvs.tm.service.TaskService;
import nlmk.nikulinvs.tm.service.UserService;
import nlmk.nikulinvs.tm.enumerated.Role;
import nlmk.nikulinvs.tm.exception.TaskNotFoundException;

import java.util.*;

public class TaskController extends AbstractController {

    private final TaskService taskService;

    private final ProjectTaskService projectTaskService;

    private final UserService userService;

    public TaskController(TaskService taskService,
                          ProjectTaskService projectTaskService,
                          UserService userService) {
        this.taskService = taskService;
        this.projectTaskService = projectTaskService;
        this.userService = userService;
    }

    public int createTask() {
        System.out.println("[CREATE TASK]");
        System.out.println("Please, enter task name");
        final String name = scanner.nextLine();
        System.out.println("Please, enter task description");
        final String description = scanner.nextLine();
        taskService.create(name, description, userService.currentUser.getId());
        System.out.println("[OK]");
        return 0;
    }

    public int updateTaskById() {
        System.out.println("[UPDATE TASK]");
        System.out.println("Please, enter task ID");
        final long id = scanner.nextLong();
        final Task task = taskService.findById(id,userService.currentUser.getId());
        if (task == null) {
            System.out.println("[FAIL]");
            return 0;
        }
        System.out.println("PLEASE, ENTER TASK NAME:");
        final String name = scanner.nextLine();
        System.out.println("Please, enter task description");
        final String description = scanner.nextLine();
        taskService.update(task.getId(), name, description,userService.currentUser.getId());
        System.out.println("[OK]");
        return 0;
    }

    public int updateTaskByIndex() {
        System.out.println("[UPDATE TASK]");
        System.out.println("Please, enter index number");
        final int index = Integer.parseInt(scanner.nextLine()) - 1;
        final Task task = taskService.findByIndex(index,userService.currentUser.getId());
        if (task == null) {
            System.out.println("[FAIL]");
            return 0;
        }
        System.out.println("PLEASE, ENTER TASK NAME:");
        final String name = scanner.nextLine();
        System.out.println("Please, enter task description");
        final String description = scanner.nextLine();
        taskService.update(task.getId(), name, description,userService.currentUser.getId());
        System.out.println("[OK]");
        return 0;
    }

    public int updateTaskByName() {
        System.out.println("[UPDATE TASK]");
        System.out.println("Please, enter task name");
        final String name = scanner.nextLine();
        final Task task = taskService.findByName(name);
        if (task == null) {
            System.out.println("[FAIL]");
            return 0;
        }
        System.out.println("PLEASE, ENTER TASK NAME:");
        final String new_name = scanner.nextLine();
        System.out.println("Please, enter task description");
        final String description = scanner.nextLine();
        taskService.update(task.getId(), new_name, description,userService.currentUser.getId());
        System.out.println("[OK]");
        return 0;
    }

    public int removeTaskById() throws ProjectNotFoundException, TaskNotFoundException {
        System.out.println("[REMOVE TASK BY ID]");
        System.out.println("[PLEASE, ENTER TASK ID:]");
        final long id = scanner.nextLong();
        final Long userId = userService.currentUser.getId();
        final Task task = taskService.removeById(id, userId);
        if (task == null) {
            throw new ProjectNotFoundException("[TASK IS NOT FOUND. REMOVE FAILED!");
        } else {
            System.out.println("[OK]");
        }
        return 0;
    }

    public int removeTaskByIndex() {
        System.out.println("[REMOVE TASK BY INDEX]");
        System.out.println("PLEASE, ENTER TASK INDEX:");
        final Scanner scanner = new Scanner(System.in);
        try {
            final int index = Integer.parseInt(scanner.nextLine()) - 1;
            final Long userId = userService.currentUser.getId();
            final Task task = taskService.removeByIndex(index, userId);
            if (task == null) {
                throw new ProjectNotFoundException("[TASK IS NOT FOUND. REMOVE CAN'T BE COMPLETE!");
            } else {
                System.out.println("[OK]");
            }
        } catch (InputMismatchException | ProjectNotFoundException e) {
            System.out.println("TASK HAVE NO PARENT PROJECT OR INPUT TYPE ERROR");
            return -1;
        }
        return 0;
    }

    public int removeTaskByName() {
        System.out.println("[REMOVE TASK BY NAME]");
        System.out.println("PLEASE, ENTER TASK NAME:");
        final String name = scanner.nextLine();
        final Task task = taskService.removeByName(name);
        if (task == null)
             System.out.println("[FAIL]");
        else System.out.println("[OK]");
        return 0;
    }

    public int clearTask() {
        System.out.println("[CLEAR TASK]");
        taskService.clear();
        System.out.println("[OK]");
        return 0;
    }

    public void viewTask(final Task task) {
        if (task == null) return;
        System.out.println("[VIEW TASK]");
        System.out.println("ID: " + task.getId());
        System.out.println("NAME: " + task.getName());
        System.out.println("DESCRIPTION: " + task.getDescription());
        System.out.println("USER_ID: " + task.getUserId());
        System.out.println("[OK]");
    }
    public void viewTaskMap(final List<Task> tasks) throws TaskNotFoundException {
        if (tasks == null || tasks.isEmpty()) {
            throw new TaskNotFoundException("[TASKS ARE NOT FOUND]");
        }
        int index = 1;
        Collections.sort(tasks, new Comparator<Task>() {
            @Override
            public int compare(Task t1, Task t2) {
                return t1.getName().compareTo(t2.getName());
            }
        });

        for (final Task task : tasks) {
            System.out.println(index + ". " + task.getId() + ": " + task.getName()+ ": " + task.getDescription()) ;
            index++;
        }
        System.out.println("[OK]");
    }

    public int viewTaskById() {
        System.out.println("ENTER, TASK ID:");
        final long id = scanner.nextLong();
        final Task task = taskService.findById(id,userService.currentUser.getId());
        viewTask(task);
        return 0;
    }

    public int viewTaskByIndex() {
        System.out.println("ENTER, TASK INDEX:");
        final int index = scanner.nextInt() - 1;
        final Task task = taskService.findByIndex(index,userService.currentUser.getId());
        viewTask(task);
        return 0;
    }

    public int viewTaskByName() {
        System.out.println("ENTER, TASK NAME:");
        final String name = scanner.nextLine();
        final Task task = taskService.findByName(name);
        viewTask(task);
        return 0;
    }

    //
    public int viewTaskMapByName() throws TaskNotFoundException {
        System.out.println("[PLEASE, ENTER TASK NAME:]");
        final String name = scanner.nextLine();
        final List<Task> tasks = taskService.findTaskMapByName(name, userService.currentUser.getId());
        viewTaskMap(tasks);
        return 0;
    }
    public int listTask() {
        System.out.println("[LIST TASK]");
        if (userService.currentUser == null) {
            System.out.println("[FAIL]");
        } else {
            if (userService.currentUser.getDisplayName().equals(Role.ADMIN.getDisplayName()))
                viewTasks(taskService.findAllOrderByName());
            else
                viewTasks(taskService.findAllByUserId(userService.currentUser.getId()));
        }
        return 0;
    }

    public void viewTasks(List<Task> tasks) {
        int index = 1;
        for (final Task task : tasks) {
            System.out.println(index + ". " + task.getId() + ": " + task.getName());
            index++;
        }
        if (index > 1) System.out.println("[OK]");
        else System.out.println("[TASKS ARE NOT FOUND]");
    }

    public int listTaskByProjectId() {
        System.out.println("[LIST TASK BY PROJECT]");
        System.out.println("PLEASE, ENTER PROJECT ID:");
        final long projectId = Long.parseLong(scanner.nextLine());
        final List<Task> tasks = taskService.findAllByProjectId(projectId);
        if (tasks.size() == 0) System.out.println("[FAIL]");
        else {
            viewTasks(tasks);
            System.out.println("[OK]");
        }
        return 0;
    }

    public int addTaskToProjectByIds() {
        System.out.println("[ADD TASK TO PROJECT BY IDS]");
        System.out.println("PLEASE, ENTER PROJECT ID:");
        final long projectId = Long.parseLong(scanner.nextLine());
        System.out.println("PLEASE, ENTER TASK ID:");
        final long taskId = Long.parseLong(scanner.nextLine());
        projectTaskService.addTaskToProject(projectId, taskId);
        System.out.println("[OK]");
        return 0;
    }

    public int removeTaskToProjectByIds() {
        System.out.println("[REMOVE TASK FROM PROJECT BY IDS]");
        System.out.println("PLEASE, ENTER PROJECT ID:");
        final long projectId = Long.parseLong(scanner.nextLine());
        System.out.println("PLEASE, ENTER TASK ID:");
        final long taskId = Long.parseLong(scanner.nextLine());
        projectTaskService.removeTaskFromProject(projectId, taskId);
        System.out.println("[OK]");
        return 0;
    }

}
