package nlmk.nikulinvs.tm.service;

import nlmk.nikulinvs.tm.entity.Project;
import nlmk.nikulinvs.tm.entity.Task;
import nlmk.nikulinvs.tm.exception.TaskNotFoundException;
import nlmk.nikulinvs.tm.repository.TaskRepository;

import java.util.List;

public class TaskService {

    private final TaskRepository taskRepository;

    public TaskService(TaskRepository taskRepository) {
        this.taskRepository = taskRepository;
    }

    public Task create(String name) {
        if (name == null || name.isEmpty()) return null;
        return taskRepository.create(name);
    }

    public Task create(String name, String description) {
        if (name == null || name.isEmpty()) return null;
        if (description == null || description.isEmpty()) return null;
        return taskRepository.create(name, description);
    }

    public Task create(String name, String description, Long userId) {
        if (name == null || name.isEmpty()) return null;
        if (description == null || description.isEmpty()) return null;
        if (userId == null) return  null;
        return taskRepository.create(name, description, userId);
    }

/*    public Task update(Long id, String name, String description) {
        if (id == null) return null;
        if (name == null || name.isEmpty()) return null;
        if (description == null || description.isEmpty()) return null;
        return taskRepository.update(id, name, description);
    }*/
public Task update(final Long id, final String name, final String description, final Long userId) {
    if (id == null) return null;
    if (name == null || name.isEmpty()) return null;
    if (description == null || description.isEmpty()) return null;
    if (userId == null) return  null;
    return taskRepository.update(id, name, description, userId);
}
    public List<Task> findAll() {
        return taskRepository.findAll();
    }

    public List<Task> findAllOrderByName() {
        return taskRepository.findAllOrderByName();
    }

    public List<Task> findAllByUserId(Long userId) {
        return taskRepository.findAllByUserId(userId);
    }

    public Task findById(final Long id, final Long userId) {
        if (id == null) return null;
        if (userId == null) return  null;
        return taskRepository.findById(id, userId);
    }

    public Task findByIndex(final int index,final Long userId) {
        if (index < 0 || index > taskRepository.findAll().size() - 1) return null;
        return taskRepository.findByIndex(index,userId);
    }

    public Task findByName(String name) {
        if (name == null || name.isEmpty()) return null;
        return taskRepository.findByName(name);
    }

    public List<Task> findTaskMapByName(final String name, final Long userId) {
        if (name == null || name.isEmpty()) return null;
        return taskRepository.findTaskMapByName(name, userId);
    }

    public Task removeById(final Long id,final Long userId) throws TaskNotFoundException {
        if (id == null) return null;
        if (userId == null) return null;
        return taskRepository.removeById(id, userId);
    }

    public Task removeByIndex(final int index, final Long userId) {
        if (index < 0 || index > taskRepository.findAll().size() - 1) return null;
        return taskRepository.removeByIndex(index,userId);
    }

    public Task removeByName(String name) {
        if (name == null || name.isEmpty()) return null;
        return taskRepository.removeByName(name);
    }

    public Task findByProjectIdAndId(Long projectId, Long id) {
        if (projectId == null || id == null) return null;
        return taskRepository.findByProjectIdAndId(projectId, id);
    }

    public List<Task> findAllByProjectId(Long projectId) {
        if (projectId == null) return null;
        return taskRepository.findAllByProjectId(projectId);
    }

    public void clear() {
        taskRepository.clear();
    }

}