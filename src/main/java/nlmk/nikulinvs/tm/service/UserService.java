package nlmk.nikulinvs.tm.service;

import nlmk.nikulinvs.tm.entity.Project;
import nlmk.nikulinvs.tm.entity.Task;
import nlmk.nikulinvs.tm.enumerated.Role;
import nlmk.nikulinvs.tm.entity.User;
import nlmk.nikulinvs.tm.repository.ProjectRepository;
import nlmk.nikulinvs.tm.repository.TaskRepository;
import nlmk.nikulinvs.tm.repository.UserRepository;

import java.util.List;

import static nlmk.nikulinvs.tm.util.HashUtils.*;

public class UserService {

    private final UserRepository userRepository;

    private final ProjectRepository projectRepository;

    private final TaskRepository taskRepository;

    public User currentUser;

    public UserService(UserRepository userRepository, ProjectRepository projectRepository, TaskRepository taskRepository) {
        this.userRepository = userRepository;
        this.projectRepository = projectRepository;
        this.taskRepository = taskRepository;
    }

    public User create(final String login, final String firstName, final String middleName, final String surName, final String password, final String roleString) {
        if (login == null || login.isEmpty()) return null;
        if (firstName == null || firstName.isEmpty()) return null;
        if (middleName == null || middleName.isEmpty()) return null;
        if (surName == null || surName.isEmpty()) return null;
        if (password == null || password.isEmpty()) return null;
        if (roleString == null || roleString.isEmpty()) return null;
        Role role = getRoleFromString(roleString);
        if (role == null) return null;
        return userRepository.create(login, firstName, middleName, surName, password, role);
    }

    public User create(final String login, final String firstName, final String middleName, final String surName, final String password) {
        if (login == null || login.isEmpty()) return null;
        if (firstName == null || firstName.isEmpty()) return null;
        if (middleName == null || middleName.isEmpty()) return null;
        if (surName == null || surName.isEmpty()) return null;
        if (password == null || password.isEmpty()) return null;
        return userRepository.create(login, firstName, middleName, surName, password);
    }

    public User update(final Long id, final String login, final String firstName, final String middleName, final String surName, final String password, final String roleString) {
        if (id == null) return null;
        if (login == null || login.isEmpty()) return null;
        if (firstName == null || firstName.isEmpty()) return null;
        if (middleName == null || middleName.isEmpty()) return null;
        if (surName == null || surName.isEmpty()) return null;
        if (password == null || password.isEmpty()) return null;
        if (roleString == null || roleString.isEmpty()) return null;
        Role role = getRoleFromString(roleString);
        return userRepository.update(id, login, firstName, middleName, surName, password, role);
    }

    public User updateByLogin(final String login, final String password, final String firstName, final String middleName, final String surName) {
        if (login == null || login.isEmpty()) return null;
        if (password == null || password.isEmpty()) return null;
        if (firstName == null || firstName.isEmpty()) return null;
        if (middleName == null || middleName.isEmpty()) return null;
        if (surName == null || surName.isEmpty()) return null;
        return userRepository.updateByLogin(login, firstName, middleName, surName, password);
    }

    public User updateById(final Long id, final String firstName, final String middleName, final String surName, final String password) {
        if (id == null) return null;
        if (password == null || password.isEmpty()) return null;
        if (firstName == null || firstName.isEmpty()) return null;
        if (middleName == null || middleName.isEmpty()) return null;
        if (surName == null || surName.isEmpty()) return null;
        return userRepository.updateById(id, firstName, middleName, surName, password);
    }

    public User updatePasswordById(final Long id, final String password) {
        if (id == null) return null;
        if (password == null) return null;
        return userRepository.updatePasswordById(id, password);
    }

    public User updatePasswordByLogin(final String login, final String password) {
        if (login == null) return null;
        if (password == null) return null;
        return userRepository.updatePasswordByLogin(login, password);
    }

    public boolean checkPassword(final User user, final String password) {
        return user.getPasswordhash().equals(hashMD5(password));
    }

    public List<User> findAll() {
        return userRepository.findAll();
    }

    public User findById(Long id) {
        if (id == null) return null;
        return userRepository.findById(id);
    }

    public User findByIndex(int index) {
        if (index < 0 || index > userRepository.findAll().size() - 1) return null;
        return userRepository.findByIndex(index);
    }

    public User findByLogin(String login) {
        if (login == null || login.isEmpty()) return null;
        return userRepository.findByLogin(login);
    }

    private Role getRoleFromString(final String roleString) {
        return Role.valueOf(roleString);
    }

    public User removeById(Long id) {
        if (id == null) return null;
        return userRepository.removeById(id);
    }

    public User removeByIndex(int index) {
        if (index < 0 || index > userRepository.findAll().size() - 1) return null;
        return userRepository.removeByIndex(index);
    }

    public User removeByLogin(String login) {
        if (login == null || login.isEmpty()) return null;
        return userRepository.removeByLogin(login);
    }

    public void clear() {
        userRepository.clear();
    }

    public Project addUserToProject(final Long projectId, final Long userId) {
        final Project project = projectRepository.findById(projectId);
        if (project == null) return null;
        final User user = userRepository.findById(userId);
        if (user == null) return null;
        project.setUserId(userId);
        return project;
    }


    public Project removeUserFromProject(final Long projectId, final Long userId) {
        final Project project = projectRepository.findById(projectId);
        if (project == null) return null;
        project.setUserId(null);
        return project;
    }

    public Task addUserToTask(final Long taskId, final Long userId) {
        final Task task = taskRepository.findById(taskId,userId);
        if (task == null) return null;
        final User user = userRepository.findById(userId);
        if (user == null) return null;
        task.setUserId(userId);
        return task;
    }

    public Task removeUserFromTask(final Long taskId, final Long userId) {
        final Task task = taskRepository.findById(taskId,userId);
        if (task == null) return null;
        task.setUserId(null);
        return task;
    }

}
