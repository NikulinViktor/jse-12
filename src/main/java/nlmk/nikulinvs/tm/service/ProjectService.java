package nlmk.nikulinvs.tm.service;

import nlmk.nikulinvs.tm.entity.Project;
import nlmk.nikulinvs.tm.exception.ProjectNotFoundException;
import nlmk.nikulinvs.tm.repository.ProjectRepository;

import java.util.List;

import java.util.List;

public class ProjectService {

    private final ProjectRepository projectRepository;

    public ProjectService(ProjectRepository projectRepository) {
        this.projectRepository = projectRepository;
    }

    public Project create(String name) {
        if (name == null || name.isEmpty()) return null;
        return projectRepository.create(name);
    }

    public Project create(String name, String description) {
        if (name == null || name.isEmpty()) return null;
        if (description == null || description.isEmpty()) return null;
        return projectRepository.create(name, description);
    }

    public Project create(String name, String description, Long userId) {
        if (name == null || name.isEmpty()) return null;
        if (description == null || description.isEmpty()) return null;
        if (userId == null) return null;
        return projectRepository.create(name, description, userId);
    }

    public Project update(Long id, String name, String description, Long userId) throws ProjectNotFoundException {
        if (id == null) return null;
        if (name == null || name.isEmpty()) return null;
        if (description == null || description.isEmpty()) return null;
        return projectRepository.update(id, name, description,userId);
    }

    public List<Project> findAll() throws ProjectNotFoundException {
        return projectRepository.findAll();
    }

    public List<Project> findAllOrderByName() {
        return projectRepository.findAllOrderByName();
    }

    public List<Project> findAllByUserId(long userId) throws ProjectNotFoundException {
        return projectRepository.findAllByUserId(userId);
    }

    public Project findById(Long id,Long userId) throws ProjectNotFoundException {
        if (id == null) return null;
        return projectRepository.findById(id,userId);
    }

    public Project findByIndex(int index,final Long userId) throws ProjectNotFoundException {
        if (index < 0 || index > projectRepository.findAll().size() - 1)
        throw new ProjectNotFoundException("Project with index = " + index + " not found");
        return projectRepository.findByIndex(index,userId);
    }

    public Project findByName(String name) {
        if (name == null || name.isEmpty()) return null;
        return projectRepository.findByName(name);
    }

    public List<Project> findProjectMapByName(final String name, final Long userId) {
        if (name == null || name.isEmpty()) return null;
        return projectRepository.findProjectMapByName(name, userId);
    }

    public Project removeById(Long id, Long userId) throws ProjectNotFoundException {
        return projectRepository.removeById(id,userId);
    }

    public Project removeByIndex(int index, final Long userId) throws ProjectNotFoundException {
        if (index < 0 || index > projectRepository.findAll().size() - 1)
        throw new ProjectNotFoundException("Project with index = " + index + " not found");
        return projectRepository.removeByIndex(index,userId);
    }

    public Project removeByName(String name) {
        if (name == null || name.isEmpty()) return null;
        return projectRepository.removeByName(name);
    }

    public void clear() {
        projectRepository.clear();
    }

}
