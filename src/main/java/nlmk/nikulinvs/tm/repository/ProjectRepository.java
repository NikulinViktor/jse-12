package nlmk.nikulinvs.tm.repository;

import nlmk.nikulinvs.tm.controller.UserController;
import nlmk.nikulinvs.tm.entity.Project;
import nlmk.nikulinvs.tm.exception.ProjectNotFoundException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.sql.SQLOutput;
import java.util.*;

import java.util.ArrayList;
import java.util.List;

public class ProjectRepository {
    private static final Comparator<Project> NAME_COMPARATOR = new Comparator<Project>() {
        @Override
        public int compare(Project p1, Project p2) {
            if (p1.getName() != null && p2.getName() != null) {
                return p1.getName().compareTo(p2.getName());
            } else if (p1.getName() == null && p2.getName() != null) {
                return -1;
            } else if (p1.getName() != null && p2.getName() == null) {
                return 1;
            } else {
                return 0;
            }
        }
    };
    private static final Logger logger = LogManager.getLogger(Project.class);
    private List<Project> projects = new ArrayList<>();
    private HashMap<String, List<Project>> projectMap = new HashMap<>();

    public Project addToMap(final Project project) {
        List<Project> projectsInMap = projectMap.get(project.getName());
        if (projectsInMap == null) projectsInMap = new ArrayList<>();
        projectsInMap.add(project);
        projectMap.put(project.getName(), projectsInMap);
        logger.info("Project " + project.getName() + " create successfully");
        return project;
    }

    public Project create(final String name) {
        final Project project = create(name,null);
        addToMap(project);
        return project;
    }

    public Project create(final String name, final String description) {
        final Project project = new Project(name, description);
        projects.add(project);
        addToMap(project);
        return project;
    }

    public Project create(final String name, final String description, final Long userId) {
        final Project project = new Project(name, description, userId);
        projects.add(project);
        addToMap(project);
        logger.info("Project " + project.getName()+ " with discription " + project.getDescription() + " create at" + System.currentTimeMillis());
        return project;
    }

    public Project update(final Long id, final String name, final String description, final Long userId) throws ProjectNotFoundException{
        Project project = findById(id, userId);
        if (project == null)
            return null;
        String oldName = project.getName();
        List<Project> projectListOld = findProjectMapByName(oldName, userId);
        if (projectListOld == null) return null;
        project.setName(name);
        project.setDescription(description);
        if (!oldName.equals(name) && projectListOld.size() > 1) {
            List<Project> projectListNew = new ArrayList<>();
            projectListNew.add(project);
            projectListOld.remove(project);
            projectMap.remove(oldName);
            projectMap.put(oldName, projectListOld);
            projectMap.put(name, projectListNew);
        } else {
            projectMap.remove(oldName);
            projectMap.put(name, projectListOld);
        }
        logger.info("Project " + project.getName() + " update successfully");
        return project;
    }

    public List<Project> findAll() throws ProjectNotFoundException {
        return projects;
    }

    public List<Project> findAllOrderByName() {
        List<Project>  copy = new ArrayList<>(projects);
        Collections.sort(copy,NAME_COMPARATOR);
        return copy;
    }

    public List<Project> findAllByUserId(final long userId) throws ProjectNotFoundException {
        final List<Project> result = new ArrayList<>();
        for (final Project project : findAll()) {
            final Long IdUser = project.getUserId();
            if (IdUser == null) continue;
            if (IdUser.equals(userId)) result.add(project);
        }
        return result;
    }

    public Project findById(final Long id) {
        for (final Project project : projects) {
            if (project.getId().equals(id)) return project;
        }
        return null;
    }

    public Project findById(final Long id, final Long userId) throws ProjectNotFoundException{

            List<Project> currentListProject;
            if (userId == null) {
                currentListProject = findAll();
            } else {
                currentListProject = findAllByUserId(userId);
            }
            if (currentListProject == null || currentListProject.size() == 0) {
                throw new ProjectNotFoundException("Project list is empty");
            }
            for (final Project project : currentListProject) {
                if (project.getId().equals(id)) return project;
            }

        throw new ProjectNotFoundException("Project with id =" + id + " is not found ");
    }

    public Project findByIndex(int index, final Long userId) throws ProjectNotFoundException{

            List<Project> result;
            if (userId == null) {
                result = findAll();
            } else {
                result = findAllByUserId(userId);
            }
            if (result == null || result.size() == 0) {
                throw new ProjectNotFoundException("Project with index = " +  index + " not found" );
            }
            if (index < 0 || index > result.size() - 1) {
                throw new ProjectNotFoundException("Input incorrect index");
            }
         for (final Project project : result) {
            if (project.getUserId().equals(userId)) return result.get(index);
            }
            throw new ProjectNotFoundException("Project with index = " + index + " not found");
    }

    public Project findByName(final String name) {
        for (final Project project : projects) {
            if (project.getName().equals(name)) return project;
        }
        return null;
    }

    //Find from Map
    public List<Project> findProjectMapByName(final String name, final Long userId) {
        try {
            if (!projectMap.containsKey(name))
                throw new ProjectNotFoundException("Project" + name + " not found by name" );
            List<Project> result = new ArrayList<>();
            if (userId == null) {
                result = projectMap.get(name);
            } else {
                for (Project project : projectMap.get(name)) {
                    if (project.getUserId().equals(userId)) {
                        result.add(project);
                    }
                }
            }
            if (result == null || result.size() == 0) {
                throw new ProjectNotFoundException("Project " + name + " not found by name " );
            }
            return result;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

public Project removeById(final Long id, final Long userId) throws ProjectNotFoundException{
    Project project = findById(id, userId);
        if (project == null)
            throw new ProjectNotFoundException("Project by id = " + id + " not found");
            List<Project> projectbyName = findProjectMapByName(project.getName(), userId);
            projectMap.remove(project.getName());
            projectbyName.remove(project);
            projectMap.put(project.getName(), projectbyName);
            return project;

}

    public Project removeByIndex(final int index, final Long userId) throws ProjectNotFoundException{
        final Project project = findByIndex(index,userId);
        if (project == null)
            throw new ProjectNotFoundException("Project by id = " + index + " not found");
        projects.remove(project);
        return project;
    }

    public Project removeByName(final String name) {
        final Project project = findByName(name);
        if (project == null) return null;
        projects.remove(project);
        return project;
    }

    public void clear() {
        projects.clear();
    }

}
