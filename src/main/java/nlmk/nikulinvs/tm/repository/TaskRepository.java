package nlmk.nikulinvs.tm.repository;
import nlmk.nikulinvs.tm.entity.Project;
import nlmk.nikulinvs.tm.entity.Task;
import nlmk.nikulinvs.tm.exception.ProjectNotFoundException;
import nlmk.nikulinvs.tm.exception.TaskNotFoundException;

import java.util.*;

public class TaskRepository {
    private static final Comparator<Task> NAME_COMPARATOR = new Comparator<Task>() {
        @Override
        public int compare(Task t1, Task t2) {
            if (t1.getName() != null && t2.getName() != null) {
                return t1.getName().compareTo(t2.getName());
            } else if (t1.getName() == null && t2.getName() != null) {
                return -1;
            } else if (t1.getName() != null && t2.getName() == null) {
                return 1;
            } else {
                return 0;
            }
        }
    };

    private List<Task> tasks = new ArrayList<>();
    private HashMap<String, List<Task>> taskMap = new HashMap<>();

    public Task addToMap(final Task task) {
        List<Task> tasksInMap = taskMap.get(task.getName());
        if (tasksInMap == null) tasksInMap = new ArrayList<>();
        tasksInMap.add(task);
        taskMap.put(task.getName(), tasksInMap);
        return task;
    }

    public Task create(final String name) {
        final Task task = create(name,null);
        addToMap(task);
        return task;
    }

    public Task create(final String name, final String description) {
        final Task task = new Task(name, description);
        tasks.add(task);
        addToMap(task);
        return task;
    }

    public Task create(final String name, final String description, final Long userId) {
        final Task task = new Task(name, description, userId);
        tasks.add(task);
        addToMap(task);
        return task;
    }

    public void clear() {
        tasks.clear();
    }

    public List<Task> findAll() {
        return tasks;
    }

    public Task update(final Long id, final String name, final String description, final Long userId) {
        Task task = findById(id, userId);
        if (task == null) return null;
        String oldName = task.getName();

        List<Task> taskListOld = findTaskMapByName(oldName, userId);
        if (taskListOld == null) return null;
        task.setName(name);
        task.setDescription(description);
        if (!oldName.equals(name) && taskListOld.size() > 1) {
            List<Task> taskListNew = new ArrayList<>();
            taskListNew.add(task);
            taskListOld.remove(task);
            taskMap.remove(oldName);
            taskMap.put(oldName, taskListOld);
            taskMap.put(name, taskListNew);
        } else {
            taskMap.remove(oldName);
            taskMap.put(name, taskListOld);
        }
        return task;
    }

    public Task findById(final Long id, final Long userId) {
        try {
            List<Task> currentListTask;
            if (userId == null) {
                currentListTask = findAll();
            } else {
                currentListTask = findAllByUserId(userId);
            }
            if (currentListTask == null || currentListTask.size() == 0) {
                throw new TaskNotFoundException("Task with ID: "+ id + "not found" );
            }
            for (final Task task : currentListTask) {
                if (task.getId().equals(id)) return task;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public Task findByProjectIdAndId(final Long projectId, final Long id) {
        for (final Task task : tasks) {
            final Long idProject = task.getProjectId();
            if (idProject == null) continue;
            if (!idProject.equals(projectId)) continue;
            if (task.getId().equals(id)) return task;
        }
        return null;
    }

/*    public Task findByIndex(int index) {
        return tasks.get(index);
    }*/
    public Task findByIndex(int index, final Long userId) {
        try {
            List<Task> result;
            if (userId == null) {
              result = findAll();
            } else {
               result = findAllByUserId(userId);
            }
            if (result == null || result.size() == 0) {
             throw new TaskNotFoundException("Task " + index + " not found" );
         }
         if (index < 0 || index > result.size() - 1) {
               throw new TaskNotFoundException("Index is uncknown");
         }
            return result.get(index);
        }  catch (Exception e) {
          e.printStackTrace();
        }
        return null;
    }

    public Task findByName(final String name) {
        for (final Task task : tasks) {
            if (task.getName().equals(name)) return task;
        }
        return null;
    }
    //Find from Map
    public List<Task> findTaskMapByName(final String name, final Long userId) {
        try {
            if (!taskMap.containsKey(name)) throw new ProjectNotFoundException("Tasks "+ name + " not found" );
            List<Task> result = new ArrayList<>();
            if (userId == null) {
                result = taskMap.get(name);
            } else {
                for (Task task : taskMap.get(name)) {
                    if (task.getUserId().equals(userId)) {
                        result.add(task);
                    }
                }
            }
            if (result == null || result.size() == 0) {
                throw new ProjectNotFoundException("Projects "+ name + " not found");
            }
            return result;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public Task removeById(final Long id,final Long userId) throws TaskNotFoundException {
        final Task task = findById(id, userId);
        if (task == null) {
            throw new TaskNotFoundException("Projects "+ id + " not found");
        }
            //return null;
        taskMap.remove(task.getId());
        tasks.remove(task);
        return task;
    }

/*    public Task removeByIndex(final int index) {
        final Task task = findByIndex(index);
        if (task == null) return null;
        tasks.remove(task);
        return task;
    }*/
    public Task removeByIndex(final int index, final Long userId) {
        Task task = findByIndex(index, userId);
        if (task == null) return null;
        tasks.remove(task);
        List<Task> taskByName = findTaskMapByName(task.getName(), userId);
        taskMap.remove(task.getName());
        taskByName.remove(task);
        taskMap.put(task.getName(), taskByName);
        return task;
}
    public Task removeByName(final String name) {
        final Task task = findByName(name);
        if (task == null) return null;
        tasks.remove(task);
        return task;
    }

    public List<Task> findAllOrderByName() {
        List<Task>  copy = new ArrayList<>(tasks);
        Collections.sort(copy,NAME_COMPARATOR);
        return copy;
    }

    public List<Task> findAllByProjectId(final Long projectId) {
        final List<Task> result = new ArrayList<>();
        for (final Task task : findAll()) {
            final Long idProject = task.getProjectId();
            if (idProject == null) continue;
            if (idProject.equals(projectId)) result.add(task);
        }
        return result;
    }

    public List<Task> findAllByUserId(final Long userId) {
        final List<Task> result = new ArrayList<>();
        for (final Task task : findAll()) {
            final Long idUser = task.getUserId();
            if (idUser == null) continue;
            if (idUser.equals(userId)) result.add(task);
        }
        return result;
    }

}

