package nlmk.nikulinvs.tm.repository;

import nlmk.nikulinvs.tm.enumerated.Role;
import nlmk.nikulinvs.tm.entity.User;

import java.util.ArrayList;
import java.util.List;

import java.util.ArrayList;
import java.util.List;

public class UserRepository {

    private List<User> users = new ArrayList<>();

    public User create(final String login, final String firstName, final String middleName, final String surName, final String password, final Role role) {
        final User user = new User(login, firstName, middleName, surName, password, role);
        users.add(user);
        return user;
    }

    public User create(final String login, final String firstName, final String middleName, final String surName, final String password) {
        final User user = new User(login, firstName, middleName, surName, password);
        user.setRole(Role.USER);
        users.add(user);
        return user;
    }

    public List<User> findAll() {
        return users;
    }

    public User update(final Long id, final String login, final String firstName, final String middleName, final String surName, final String password, final Role role) {
        final User user = findById(id);
        if (user == null) return null;
        user.setId(id);
        user.setLogin(login);
        user.setFirstName(firstName);
        user.setMiddleName(middleName);
        user.setSurName(surName);
        user.setPassword(password);
        user.setRole(role);
        return user;
    }

    public User updateByLogin(final String login, final String firstName, final String middleName, final String surName, final String password) {
        final User user = findByLogin(login);
        if (user == null) return null;
        user.setPassword(password);
        user.setFirstName(firstName);
        user.setMiddleName(middleName);
        user.setSurName(surName);
        return user;
    }

    public User updatePasswordById(final Long id, final String password) {
        final User user = findById(id);
        if (user == null) return null;
        user.setPassword(password);
        return user;
    }

    public User updatePasswordByLogin(final String login, final String password) {
        final User user = findByLogin(login);
        if (user == null) return null;
        user.setPassword(password);
        return user;
    }

    public User updateById(final Long id, final String firstName, final String middleName, final String surName, final String password) {
        final User user = findById(id);
        if (user == null) return null;
        user.setPassword(password);
        user.setFirstName(firstName);
        user.setMiddleName(middleName);
        user.setSurName(surName);
        return user;
    }

    public User updateRoleById(final Long id, final Role role) {
        final User user = findById(id);
        if (user == null) return null;
        user.setRole(role);
        return user;
    }

    public User updateRoleByLogin(final String login, final Role role) {
        final User user = findByLogin(login);
        if (user == null) return null;
        user.setRole(role);
        return user;
    }

    public User findById(final Long id) {
        for (final User user : users) {
            if (user.getId().equals(id)) return user;
        }
        return null;
    }

    public User findByIndex(int index) {
        return users.get(index);
    }

    public User findByLogin(final String login) {
        for (final User user : users) {
            if (user.getLogin().equals(login)) return user;
        }
        return null;
    }

    public User removeById(final Long id) {
        final User user = findById(id);
        if (user == null) return null;
        users.remove(user);
        return user;
    }

    public User removeByIndex(final int index) {
        final User user = findByIndex(index);
        if (user == null) return null;
        users.remove(user);
        return user;
    }

    public User removeByLogin(final String login) {
        final User user = findByLogin(login);
        if (user == null) return null;
        users.remove(user);
        return user;
    }

    public void clear() {
        users.clear();
    }

}